<?php

namespace App\Http\Commands;
use App\Http\Models\Url;
use Illuminate\Console\Command;
use Exception;

class UrlSeeker extends Command
{
    protected $signature = 'url-seeker';

    protected $description = 'im searching for something';

    public function handle()
    {
        try {
            $unprocessedUrl = Url::where(['is_processed' => 0])->get();
            foreach ($unprocessedUrl as $url) {
                $pattern = "/<title[^>]*>(.*?)<\/title>/is";
                $response = file_get_contents($url->url);
                preg_match_all($pattern, $response, $matches);
                if($matches[1][0]){
                    $url->title = $matches[1][0];
                    $url->is_processed = 1;
                    $url->save();
                }
            }
            $this->info('Url were processed successfully.');
        } catch (Exception $exception) {
            $this->info('Something went wrong, please try again.');
        }
    }
}
