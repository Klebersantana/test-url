<?php

namespace App\Http\Controllers;

use App\Http\Models\Url;
use Illuminate\Http\Request;
use Exception;

class UrlController extends Controller
{
    public function index()
    {
        try {
            $processedUrls = Url::where(['is_processed' => 1])->orderBy('hits', 'desc')->limit(100)->get()->toArray();
            return response()->json(['items' => $processedUrls], 200);
        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());
        }
    }

    public function store(Request $request)
    {
        try {
            $validatedData = validator($request->all(), ['url' => ['required']])->validate();
            $url = new Url();
            $url->url = $validatedData['url'];
            $url->identifier = str_random(4);
            $url->is_processed = 0;
            $url->save();

            return response()->json(['message' => 'Url savec successfully.', 'idenfier' => $url->identifier], 200);
        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());
        }
    }

    public function redirectTo($identifier)
    {
        try {
            if ($url = Url::where('identifier', $identifier)->first()) {
                ++$url->hits;
                $url->save();
                return redirect($url->url);
            }

            return response()->json(['message' => 'Invalid identifier was sent.'], 404);
        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());
        }

    }
}
