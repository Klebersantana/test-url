<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $fillable = ['title', 'url', 'identifier', 'is_processed', 'hits'];

    protected $table = 'url';

    public $timestamps = false;
}
