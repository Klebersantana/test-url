<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Commands\UrlSeeker;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        'url-seeker' => UrlSeeker::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('url-seeker')->everyMinute()->appendOutputTo(storage_path('logs/url-seeker.log'));
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
