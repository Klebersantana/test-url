<?php

use Illuminate\Http\Request;

Route::resource('/url', 'UrlController');
Route::get('/{identifier}', 'UrlController@redirectTo');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
