Hey guys, adding some steps to make it works:


- create a database
- Set up your database in the ".env" file
- run migrations
- Sending a post to "/url" with the desired web site link, will store it on database as an unprocessed url:
i.e: {"url": "https://www.google.com/"}
- Sending a get with the desired "identifier" will count it as an accessed url and redirects you to the web site
- Sending a get to "/url" will show the top 100 URLs most frequently accessed
- By running this command "php artisan schedule:run >> /dev/null 2>&1" you will be able to run that specified routine
that crawls web sites titles and mark it as a processed url
- Let me know if there's any question about it
