Hey guys, adding some steps to make it works:
1. create a database
2. Set up your database in the ".env" file
3. run migrations
4. Sending a post to "/url" with the desired web site link, will store it on database as an unprocessed url:
   i.e: {"url": "https://www.google.com/"}
5. Sending a get with the desired "identifier" will count it as an accessed url and redirects you to the web site
6. Sending a get to "/url" will show the top 100 URLs most frequently accessed
7. By running this command "php artisan schedule:run >> /dev/null 2>&1" you will be able to run that specified routine 
that crawls web sites titles and mark it as a processed url
8. Let me know if there's any question about it 

